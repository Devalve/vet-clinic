package data;

import entity.Appointment;
import entity.Doctor;
import entity.Patient;

import java.util.ArrayList;
import java.util.List;

public class Storage {
    public static List<Patient> patients = new ArrayList<>();
    public static List<Doctor> doctors = new ArrayList<>();
    public static List<Appointment> appointments = new ArrayList<>();
}
