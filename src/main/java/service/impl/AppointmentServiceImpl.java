package service.impl;

import data.Storage;
import entity.Appointment;
import enums.AppointmentStatus;
import service.AppointmentService;

import java.util.Calendar;

public class AppointmentServiceImpl implements AppointmentService {
    @Override
    public String createAppointment(String patientFio, Calendar registrationDate, AppointmentStatus status) {
        Storage.appointments.add(new Appointment(patientFio, registrationDate, status));
        return "Appointment was added!";
    }
}
