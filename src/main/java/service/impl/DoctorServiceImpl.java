package service.impl;

import data.Storage;
import entity.Doctor;
import service.DoctorService;

public class DoctorServiceImpl implements DoctorService {
    @Override
    public String createDoctor(String fio) {
        Storage.doctors.add(new Doctor(fio));
        return "Dr " + fio + " was added!";
    }
}
