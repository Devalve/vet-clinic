package service.impl;

import data.Storage;
import entity.Patient;
import service.PatientService;

import java.util.Calendar;

public class PatientServiceImpl implements PatientService {
    @Override
    public void createPatient(String fio, Calendar registrationDate) {
        Storage.patients.add(new Patient(fio, registrationDate));
        System.out.println("Patient was added!");
    }

    @Override
    public void showAllPatients() {
        for (Patient p : Storage.patients) {
            System.out.println("Id: " + p.getId() + "\n" + "FIO: " + p.getFio() + "\n" + "Registration date: " + p.getRegistrationDate());
            System.out.println();
        }
    }
}
