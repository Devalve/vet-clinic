package service;

import java.util.Calendar;

public interface PatientService {
    void createPatient(String fio, Calendar registrationDate);

    void showAllPatients();
}
