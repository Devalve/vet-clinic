package service;

import enums.AppointmentStatus;

import java.util.Calendar;

public interface AppointmentService {
    String createAppointment(String patientFio, Calendar registrationDate, AppointmentStatus status);
}
