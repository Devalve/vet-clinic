package entity;

import enums.AppointmentStatus;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.Calendar;

@Getter
@Setter
@AllArgsConstructor
public class Appointment {

    private String patientFio;

    private Calendar registrationDate;

    private AppointmentStatus status;
}
